const fs = require('fs').promises;
const { exec } = require('child_process');
const util = require('util');

const execAsync = util.promisify(exec);

async function processFile() {
    try {
        // Read the dotFiles_Home.txt file
        const fileContent = await fs.readFile('dotFiles_Home.txt', 'utf-8');
        const files = fileContent.split('\n').filter(line => line.trim() !== '');

        // Process each file/directory
        for (const file of files) {
            console.log(`Processing: ${file}`);
            
            try {
                const { stdout, stderr } = await execAsync(`chezmoi add -n -v "${file}"`);

                // Log output to process.log
                if (stdout) {
                    await fs.appendFile('process.log', stdout);
                }

                // Log errors to errors.log
                if (stderr) {
                    await fs.appendFile('errors.log', stderr);
                }
            } catch (error) {
                await fs.appendFile('errors.log', `Error processing ${file}: ${error.message}\n`);
            }
        }
    } catch (error) {
        console.error('Failed to read the dotFiles_Home.txt file.', error);
    }
}

processFile();

