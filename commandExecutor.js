const { exec } = require('child_process');
const util = require('util');

const execAsync = util.promisify(exec);

/**
 * Executes a shell command.
 *
 * @param {string} cmd - The shell command to execute.
 * @returns {Promise<string>} - The command's stdout.
 * @throws {Error} - If the command results in stderr.
 */ 
exports.execCommand = async function execCommand(cmd) {
    const { stdout, stderr } = await execAsync(cmd);

    if (stderr) {
        throw new Error(`Error executing command: ${stderr}`);
    }

    return stdout;
}

