const fs = require('fs').promises;

/**
 * Checks if a file exists.
 *
 * @param {string} filepath - Path to the file.
 * @returns {Promise<boolean>} - True if the file exists, false otherwise.
 */
async function fileExists(filepath) {
    try {
        await fs.access(filepath);
        return true;
    } catch {
        return false;
    }
}

module.exports = fileExists;
