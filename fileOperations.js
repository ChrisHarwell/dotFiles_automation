const fs = require('fs').promises;
const { execCommand } = require('./commandExecutor');
const fileExists = require('./fileChecker');

/**
 * Appends data to a specified file.
 *
 * @param {string} filename - The name of the file to append data to.
 * @param {string} data - The data to append to the file.
 * @returns {Promise<void>}
 * @throws {Error} - If there's an issue with file operations.
 */
async function appendToFile(filename, data) {
    try {
        await fs.appendFile(filename, data);
    } catch (error) {
        throw new Error(`Failed to append data to ${filename}: ${error.message}`);
    }
}

exports.generateDotFilesHome = async function() {
    const cmd = 'exa --all --no-icons --no-time --no-user --no-filesize --no-permissions --classify --list-dirs --oneline --group-directories-first .* >> dotFiles_Home.txt';
    await execCommand(cmd);
};

exports.ensureInChezmoiFileExists = async function() {
    if (!await fileExists('in_chezmoi.txt')) {
        await execCommand('chezmoi managed -i dirs,files >> in_chezmoi.txt');
    }
};

exports.generateDifferenceFile = async function() {
    await execCommand('sort in_chezmoi.txt > in_chezmoi_sorted.txt');
    await execCommand('sort dotFiles_Home.txt > dotFiles_Home_sorted.txt');
    await execCommand('comm -23 in_chezmoi_sorted.txt dotFiles_Home_sorted.txt > diff.txt');
};


exports.processFilesFromDiff = async function() {
    const fs = require('fs').promises;
    const fileContent = await fs.readFile('diff.txt', 'utf-8');
    const files = fileContent.split('\n').filter(line => line.trim() !== '');

    for (const file of files) {
        try {
            const stdout = await execCommand(`chezmoi add -n -v "~/${file}"`);
            if (stdout) {
                await appendToFile('process.log', stdout);
            }
        } catch (error) {
            await appendToFile('errors.log', error.message);
        }
    }
};

