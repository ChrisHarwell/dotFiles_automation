const {
  generateDotFilesHome,
  ensureInChezmoiFileExists,
  generateDifferenceFile,
  processFilesFromDiff
} = require('./fileOperations');

// Execution sequence
generateDotFilesHome()
  .then(ensureInChezmoiFileExists)
  .then(generateDifferenceFile)
  .then(processFilesFromDiff)
  .catch(error => {
    console.error(`Failed to run commands: ${error.message}`);
  });
